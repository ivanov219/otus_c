﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _01
{
    /// <summary>
    /// Координата на круге - градус, минута, секунда
    /// Всегда нормализовна: 0 <=секунд < 60, 0<= минут < 60, 0<= градус < 360
    /// </summary>
    public class CircleCoordinate
    {

        // Градус, Минуты, Секунды
        public int Degree { get; private set; }
        public int Minutes { get; private set; }
        public int Seconds { get; private set; }

        public CircleCoordinate(int degree = 0, int minutes = 0, int seconds = 0)
        {
            this.Seconds = seconds;
            this.Minutes = minutes;
            this.Degree = degree;

            Normalize();
        }

        // В секундах
        public int ToSeconds() { return (Degree * 60 + Minutes) * 60 + Seconds; }

        /// <summary>
        /// Нормализовать координату 
        ///  секунды <= 60
        ///  минуты  <= 60
        ///  градусы <= 360
        /// </summary>
        private void Normalize() { 
            // переводим в секунды
            int totalSeconds = ToSeconds();

            // отрицательные переводим в положительные +360 градусов
            while (totalSeconds < 0) 
                totalSeconds += 360 * 60 * 60;

            // переводим обратно
            this.Degree = (totalSeconds / (60 * 60)) % 360;
            this.Minutes = (totalSeconds / ( 60) ) % 60;
            this.Seconds = totalSeconds % ( 60);
        }

        // Сравнение
        public static bool operator ==(CircleCoordinate a, CircleCoordinate b) 
            { return a.Degree == b.Degree && a.Minutes == b.Minutes && a.Seconds == b.Seconds; }
        public static bool operator !=(CircleCoordinate a, CircleCoordinate b) 
            { return !(a == b); }

        // Координата + Координата = Новая координата
        public static CircleCoordinate operator +(CircleCoordinate a, CircleCoordinate b)  
            {   return a.Add(b.Degree, b.Minutes, b.Seconds);  }

        // Расстояние между координатами в секундах. Может быть отрицательным
        public static int operator -(CircleCoordinate a, CircleCoordinate b) 
            { return a.ToSeconds() - b.ToSeconds(); }


        // Грудус.Минуты'Секунды''
        public override string ToString()
        {
            return String.Format("{0}.{1:00}'{2:00}''", Degree, Minutes, Seconds);
        }

    }

    /// <summary>
    /// Расширения для работы с координатами
    /// </summary>
    public static class CircleCoordinateExtension
    {
        public static CircleCoordinate Add(this CircleCoordinate coordinate, int degree, int minutes = 0, int seconds = 0)
        {
            return new CircleCoordinate(coordinate.Degree + degree, coordinate.Minutes + minutes, coordinate.Seconds + seconds);
        }

        public static CircleCoordinate AddDegree(this CircleCoordinate coordinate, int degree) { return coordinate.Add(degree); }
        public static CircleCoordinate AddMinutes(this CircleCoordinate coordinate, int minutes) { return coordinate.Add(0, minutes); }
        public static CircleCoordinate AddSeconds(this CircleCoordinate coordinate, int seconds) { return coordinate.Add(0, 0, seconds); }

    
    }
}
