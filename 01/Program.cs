﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _01
{
    class Program
    {

        static void Main(string[] args)
        {
            var coord1 = new CircleCoordinate();
            Console.WriteLine(coord1);

            var coord2 = new CircleCoordinate(5, 0, 10);
            Console.WriteLine(coord2);

            var coord3 = new CircleCoordinate(-10,-10,-10);
            Console.WriteLine(coord3);
            
            Console.ReadKey();
        }
    }
}
