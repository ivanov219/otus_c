﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;

using _01;

namespace _01_UnitTests
{
    [TestClass]
    public class CircleCoordinateTests
    {
        [TestMethod]
        public void Test_Constructor_Default()
        {
            var coord = new CircleCoordinate();
            var coordZero = new CircleCoordinate(0, 0, 0);
            Assert.IsTrue(coord == coordZero);
        }

        [TestMethod]
        public void Test_Constructor_OverRange_Seconds()
        {
            var coord = new CircleCoordinate(0, 0, 65);
            var coordExpected = new CircleCoordinate(0, 1, 5);
            Assert.IsTrue(coord == coordExpected);
        }

        [TestMethod]
        public void Test_Constructor_OverRange_Minutes()
        {
            var coord = new CircleCoordinate(0,  65, 0);
            var coordExpected = new CircleCoordinate(1, 5, 0);
            Assert.IsTrue(coord == coordExpected);
        }

        [TestMethod]
        public void Test_Constructor_OverRange_Degree()
        {
            var coord = new CircleCoordinate(370, 0, 0);
            var coordExpected = new CircleCoordinate(10, 0, 0);
            Assert.IsTrue(coord == coordExpected);
        }


        [TestMethod]
        public void Test_Constructor_OverRange_MinutesAndSeconds()
        {
            var coord = new CircleCoordinate(0, 65, 65);
            var coordExpected = new CircleCoordinate(1, 6, 5);
            Assert.IsTrue(coord == coordExpected);
        }

        [TestMethod]
        public void Test_Constructor_OverRange_All()
        {
            var coord = new CircleCoordinate(365, 65, 65);
            var coordExpected = new CircleCoordinate(6, 6, 5);
            Assert.IsTrue(coord == coordExpected);
        }

        [TestMethod]
        public void Test_Constructor_Negative()
        {
            var coord = new CircleCoordinate(-10,0,0);
            var coordZero = new CircleCoordinate(350, 0, 0);
            Assert.IsTrue(coord == coordZero);
        }

        [TestMethod]
        public void Test_Add_Seconds()
        {
            var coord = new CircleCoordinate(0, 0, 0);
            var coordNew = coord.AddSeconds(65);
            var coordExpected = new CircleCoordinate(0, 1, 5);
            Assert.IsTrue(coordNew == coordExpected);
        }

        [TestMethod]
        public void Test_Add_Minutes()
        {
            var coord = new CircleCoordinate(0, 0, 0);
            var coordNew = coord.AddMinutes(65);
            var coordExpected = new CircleCoordinate(1, 5, 0);
            Assert.IsTrue(coordNew == coordExpected);
        }

        [TestMethod]
        public void Test_Add_Degree()
        {
            var coord = new CircleCoordinate(357, 0, 0);
            var coordNew = coord.AddDegree(8);
            var coordExpected = new CircleCoordinate(5, 0, 0);
            Assert.IsTrue(coordNew == coordExpected);
        }

        [TestMethod]
        public void Test_Add_Coordinates()
        {
            var coord1 = new CircleCoordinate(359, 59, 59);
            var coord2 = new CircleCoordinate(0, 0, 1);
            var coordNew = coord1 + coord2;
            var coordExpected = new CircleCoordinate(0, 0, 0);
            Assert.IsTrue(coordNew == coordExpected);
        }

        [TestMethod]
        public void Test_Add_Coordinates_WithOverRange()
        {
            var coord1 = new CircleCoordinate(350, 50, 40);
            var coord2 = new CircleCoordinate(40, 25, 35);
            var coordNew = coord1 + coord2;
            var coordExpected = new CircleCoordinate(31, 16, 15);
            Assert.IsTrue(coordNew == coordExpected);
        }

        [TestMethod]
        public void Test_Sub_Coordinates()
        {
            var coord1 = new CircleCoordinate(10, 20, 30);
            var coord2 = new CircleCoordinate(5, 5, 5);
            int distance = coord1 - coord2;
            Assert.AreEqual(distance, (5 * 60 + 15) * 60 + 25);
        }

        [TestMethod]
        public void Test_Sub_Coordinates_Negatve()
        {
            var coord1 = new CircleCoordinate(5, 5, 5);
            var coord2 = new CircleCoordinate(10, 20, 30);
            int distance = coord1 - coord2;
            Assert.AreEqual(distance, (-5 * 60 + -15) * 60 + -25);
        }

        [TestMethod]
        public void Test_ToSeconds()
        {
            var coord = new CircleCoordinate(350, 50, 40);
            Assert.AreEqual(coord.ToSeconds(), (coord.Degree * 60 + coord.Minutes) *60 + coord.Seconds);
        }

        [TestMethod]
        public void Test_ToString()
        {
            var coord = new CircleCoordinate(357, 52, 63);
            Assert.AreEqual(coord.ToString(), "357.53'03''");
        }
       

    }
}
